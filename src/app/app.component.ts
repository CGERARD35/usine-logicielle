import {Component} from '@angular/core';
import { RouterOutlet } from '@angular/router';
import {ComponentModule} from "./component/component.module";

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [RouterOutlet, ComponentModule],
  templateUrl: './app.component.html',
  styleUrl: './app.component.scss'
})
export class AppComponent {
  title = 'StreamingHacking';

}
