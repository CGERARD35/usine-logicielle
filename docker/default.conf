server {
    listen       80;
    listen  [::]:80;
    server_name  localhost;

    client_max_body_size 512M;

    root   /usr/share/nginx/html;

    # Index Files
    index index.html index.htm;

    # Gzip Settings
    gzip on;
    gzip_proxied any;
    gzip_types text/plain text/css application/javascript application/json application/x-javascript text/xml application/xml application/xml+rss text/javascript;

    # Logging Settings
    access_log /var/log/nginx/access.log;
    error_log /var/log/nginx/error.log;

    location / {
        try_files $uri $uri/ /index.html;

        # Browser Caching
        location ~* \.(jpg|jpeg|png|gif|ico|css|js)$ {
            expires 30d;
        }
    }

    # Custom Error Page
    error_page 500 502 503 504 /50x.html;
    location = /50x.html {
        root /usr/share/nginx/html;
    }

    # Disable server tokens in response headers
    server_tokens off;
}
